using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Nextensions
{
	public static partial class Draw
	{
		public static void GizmosCircle(Vector3 origin, Vector3 normal, float radius, int steps = 24)
		{
			Vector3 p1, p2;
			float arc = 360f / steps;
			Quaternion rot = Quaternion.AngleAxis(arc, normal);
			Vector3 spokeDirection = Vector3.Cross (normal, Vector3.right);
			// check if the normal points as Vector3.right
			if ( Vector3.Angle(normal,Vector3.right) < Mathf.Epsilon) spokeDirection = Vector3.Cross (normal, Vector3.forward);
			if ( Vector3.Angle(normal,Vector3.right) >= 180 ) spokeDirection = Vector3.Cross (normal, Vector3.forward);
			//		if ( Mathf.Abs (spokeDirection.sqrMagnitude - 0) < Mathf.Epsilon) spokeDirection = Vector3.Cross (normal, Vector3.forward);

			spokeDirection=spokeDirection.normalized;

			p2 = origin + spokeDirection * radius;

			for (int i = 0; i < steps; i++)
			{
				p1 = p2;
				//Gizmos.DrawRay(topHingePosition.position, _gizmosCircleDir * gizmoRadius); // spokes
				spokeDirection = rot * spokeDirection;
				p2 = origin + spokeDirection * radius;
				Gizmos.DrawLine(p1, p2);
			}
		}
	}
}