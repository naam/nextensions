using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;

namespace Nextensions
{
	public static partial class Nextensions
	{
		//
		// string helpers
		//


		public static bool SameAs( this string a, string b ) => a.ToUpper()==b.ToUpper();


		//
		// transform helpers
		//
		public static void Zero( this Transform transform )
		{
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			transform.localScale = Vector3.one;
		}


		public static void Pose( this Transform transform, Transform target )
		{
			transform.position = target.position;
			transform.rotation = target.rotation;
		}


		public static void Mimic( this Transform transform, Transform target )
		{
			var p = transform.parent;
			transform.SetParent( target );
			transform.Zero();
			transform.SetParent( p, true );
		}


		public static Vector3 EulerWrap( this Vector3 euler )
		{
			while( euler.x> 180 ) euler.x-=360;
			while( euler.x<-180 ) euler.x+=360;
			while( euler.y> 180 ) euler.y-=360;
			while( euler.y<-180 ) euler.y+=360;
			while( euler.z> 180 ) euler.z-=360;
			while( euler.z<-180 ) euler.z+=360;
			return euler;
		}

		public static Vector3 Local( this Transform transform, Vector3 pos ) =>
			transform.InverseTransformPoint( pos );

		public static float Local( this Transform transform, float radius ) =>
			transform.InverseTransformVector( Vector3.right *radius ).magnitude;

		/// <summary> Returns the global 'smartforward' vector of the given transform in the 
		/// space of flatSpace. Useful to get a 'user-forward' in a VR context </summary>
		public static Vector3 SmartForward( this Transform transform, Transform flatSpace = null )
		{
			Vector3 fwd = 
				flatSpace!=null
					? flatSpace.InverseTransformDirection( transform.forward )
					: transform.forward;
			var flatFwd = fwd;
			flatFwd.y = 0;
			flatFwd = flatFwd.normalized;
			Vector3 up = 
				flatSpace!=null
					? flatSpace.InverseTransformDirection( transform.up )
					: transform.up;
			float useUpFactor = Mathf.InverseLerp( 0, 90, Vector3.Angle(fwd,flatFwd) );
			if( fwd.y>0 ) up = -up;
			Vector3 smartForward = Vector3.Lerp( fwd, up, useUpFactor ).normalized;
			smartForward.y=0;
			return 
				flatSpace!=null
					? flatSpace.TransformDirection(smartForward).normalized
					: smartForward.normalized;
		}


		//
		// Vector helpers
		//
		public static Vector3 Wrap( this Vector3 p, float radius )
		{
			if( p.x> radius ) p.x-=2*radius;
			if( p.x<-radius ) p.x+=2*radius;
			if( p.y> radius ) p.y-=2*radius;
			if( p.y<-radius ) p.y+=2*radius;
			if( p.z> radius ) p.z-=2*radius;
			if( p.z<-radius ) p.z+=2*radius;
			return p;
		}


		public static Vector3 Rotate( this Vector3 v, float angle, Vector3 axis ) =>
			Quaternion.AngleAxis( angle, axis ) *v;


		public static Vector3 Horizontal( this Vector3 v ) => v.ScaleBy(1,0,1);
		public static Vector3 ScaleBy( this Vector3 v, Vector3 d) => Vector3.Scale(v,d );
		public static Vector3 ScaleBy( this Vector3 v, float x, float y, float z ) =>
			Vector3.Scale( v, new Vector3(x,y,z) );


		public static float MinMaxRandom( this Vector2 v ) => Random.Range(v.x,v.y);

		public static Vector3 TwiddleYZ( this Vector2 v ) => new Vector3(v.x,0,v.y);


		// 
		// float helpers
		//
		public static bool IsBetween( this float value, float min, float max ) => value>min&&value<max;
		public static bool IsBetweenInc( this float value, float min, float max ) => value>=min&&value<=max;


		//
		// Bounds helpers
		//
		public static Bounds TransformBounds( this Transform transform, Bounds bounds )
		{
			{
				// method 1: should suffice
				var result = new Bounds();
				result.center = transform.TransformPoint( bounds.center );
				result.Encapsulate( transform.TransformPoint( bounds.min ) );
				result.Encapsulate( transform.TransformPoint( bounds.max ) );
				return result;
			}

			{
				// method 2: more robust but more involved
				var result = new Bounds( transform.TransformPoint(bounds.center), transform.TransformVector(bounds.size) );
				result.Encapsulate( transform.TransformPoint( new Vector3(bounds.min.x,0,0) ) );
				result.Encapsulate( transform.TransformPoint( new Vector3(bounds.max.x,0,0) ) );
				result.Encapsulate( transform.TransformPoint( new Vector3(0,bounds.min.y,0) ) );
				result.Encapsulate( transform.TransformPoint( new Vector3(0,bounds.max.y,0) ) );
				result.Encapsulate( transform.TransformPoint( new Vector3(0,0,bounds.min.z) ) );
				result.Encapsulate( transform.TransformPoint( new Vector3(0,0,bounds.max.z) ) );
				return result;
			}
		}


		public static Bounds InverseTransformBounds( this Transform transform, Bounds bounds )
		{
			var result = new Bounds();
			result.center = transform.InverseTransformPoint( bounds.center );
			result.Encapsulate( transform.InverseTransformPoint( bounds.min ) );
			result.Encapsulate( transform.InverseTransformPoint( bounds.max ) );
			return result;
		}

		public static Vector3 ToVector3( this Vector4 value ) => new Vector3( value.x, value.y, value.z );
		public static Bounds ToBounds( this Vector4 value ) => 
			new Bounds( value.ToVector3(), Vector3.one*value.w );


		//
		// component helpers
		//
		public static T AddOrGetComponent<T>( this GameObject op ) where T: Component
			=> op.transform.CreateOrAddComponent<T>();
		public static T AddOrGetComponent<T>( this Component component ) where T: Component
			=> component.CreateOrAddComponent<T>();
		/// <summary> BADLY NAMED, please use AddOrGetComponent </summary>
		public static T CreateOrAddComponent<T>( this GameObject op ) where T: Component
			=> op.transform.CreateOrAddComponent<T>();
		/// <summary> BADLY NAMED, please use AddOrGetComponent </summary>
		public static T CreateOrAddComponent<T>( this Component component ) where T : Component
		{
			T result = component.GetComponent<T>();
			if( result != null ) return result;
			return component.gameObject.AddComponent<T>();
		}


		//
		// search and tag helpers
		//
		public static string Tag( this string str, string tag )
		{
			if( str.ToUpper().Contains(tag.ToUpper() ) ) return str;
			if( tag.StartsWith("@") == false ) tag = $"@{tag}";
			return $"{str}_{tag}";
		}

		public static GameObject FindByTaggedName( this GameObject op, string tag ) =>
			op.transform.FindByTaggedName( tag )?.gameObject;
		public static T FindByTaggedName<T>( this GameObject op, string tag ) where T: Component =>
			op.transform.FindByTaggedName<T>( tag );
		public static Transform FindByTaggedName( this Component component, string tag ) =>
			component.GetComponentsInChildren<Transform>(true).FirstOrDefault( a=> a.gameObject.name.ToUpper().Contains(tag.ToUpper()) );
		public static T FindByTaggedName<T>( this Component component, string tag ) where T : Component =>
			component.GetComponentsInChildren<Transform>(true).FirstOrDefault( a=> a.gameObject.name.ToUpper().Contains(tag.ToUpper()) )?.GetComponent<T>();
		public static IEnumerable<Transform> FindByTaggedNameAll( this Component component, string tag ) =>
			component.GetComponentsInChildren<Transform>(true).Where( a=> a.gameObject.name.ToUpper().Contains(tag.ToUpper()) );
		public static IEnumerable<T> FindByTaggedNameAll<T>( this GameObject op, string tag ) where T : Component =>
			op.GetComponentsInChildren<T>(true).Where( a=> a.gameObject.name.ToUpper().Contains(tag.ToUpper()) );
		public static IEnumerable<T> FindByTaggedNameAll<T>( this Component component, string tag ) where T : Component =>
			component.GetComponentsInChildren<T>(true).Where( a=> a.gameObject.name.ToUpper().Contains(tag.ToUpper()) );
		
		public static string GetTag( this string str )
		{
			var matches = Regex.Match( str, @"(@[a-zA-Z0-9_]+)" );
			if( matches.Success ) return matches.Groups[1].Value;
			else return "";
		}

		public static string GetCleanTag( this string str )
		{
			var matches = Regex.Match( str, @"@([a-zA-Z0-9_]+)" );
			if( matches.Success ) return matches.Groups[1].Value;
			else return "";
		}

		public static string SplitTag( this string str, string startOfTag )
		{
			var result = Regex.Replace( str.GetTag().ToUpper(), startOfTag.ToUpper(), "" );
			if( result.Length == 0 ) return null;
			else return "@"+result;
		}

		//
		// C# type helpers
		//
		public static HashSet<T> ToHashSet<T>( this T[] array ) => new HashSet<T>(array);

		public static T RandomPull<T>( this List<T> list )
		{
			var result = list[Random.Range(0,list.Count)];
			list.Remove(result);
			return result;
		}

		public static T Pull<T>( this List<T> list )
		{
			var result = list[0];
			list.RemoveAt(0);
			return result;
		}


		public static bool IsEmpty( this string str )
			=> str==null || str.Length == 0 || Regex.IsMatch(str,@"^\s*$");
		public static bool IsPresent( this string str ) => !str.IsEmpty();


		public static string SplitAtCapitals( this string value )
		{
			var result = "";
			foreach( var letter in value )
				result += (System.Char.IsUpper(letter)?" ":"") + letter;
			result = result.Trim();
			return result;
		}


		/// <summary> Split on pure newlines, ignoring newlines that belong to fields. Keeps quotation marks intact. </summary>
		public static string[] CSVLines(this string str)
		{
			var lines = new List<string>();
			var line = new System.Text.StringBuilder();
			bool insideField = false;
			for (int i = 0; i < str.Length; i++)
			{
				char c = str[i];
				if (c == '\n' && !insideField)
				{
					// Add the current line to the list of lines and start a new line
					lines.Add(line.ToString());
					line.Clear();
				}
				else if (c == '"')
				{
					if (insideField)
					{
						if (i < str.Length - 1 && str[i + 1] == '"')
						{
							// Double quote within a quoted field, treat it as a literal quote
							line.Append('"');
							line.Append('"');
							i++;
						}
						else
						{
							// End of the quoted field
							line.Append('"');
							insideField = false;
						}
					}
					else
					{
						// Start of a quoted field
						line.Append('"');
						insideField = true;
					}
				}
				else
				{
					// Add the current character to the current line
					line.Append(c);
				}
			}
			// Add the final line to the list of lines
			lines.Add(line.ToString());
			return lines.ToArray();
		}


		/// <summary> Splits a CSV line into its fields, keeping in-field quotes, newlines and commas intact. It does expect the 
		/// line to have qoutes around the fields that countain commas or quotation marks or newlines. </summary>
		public static string[] CSVFields( this string str )
		{
			var fields = new List<string>();
			var currentField = new System.Text.StringBuilder();
			var insideQuotedField = false;

			for (int i = 0; i < str.Length; i++)
			{
				char c = str[i];
				char? nextChar = i < str.Length - 1 ? str[i + 1] : (char?)null;

				if (c == '"' && !insideQuotedField)
				{
					insideQuotedField = true;
				}
				else if (c == ',' && !insideQuotedField)
				{
					fields.Add(currentField.ToString());
					currentField.Clear();
				}
				else if (c == '"' && insideQuotedField)
				{
					if (nextChar == '"')
					{
						// Handle escaped quote
						currentField.Append('"');
						i++;
					}
					else
					{
						// End of quoted field
						insideQuotedField = false;
					}
				}
				else if (c == '\r' && nextChar == '\n' && !insideQuotedField)
				{
					// Ignore CRLF at end of field
					i++;
				}
				else
				{
					currentField.Append(c);
				}
			}

			fields.Add(currentField.ToString().Trim());

			var dbg = "";
			foreach( var f in fields ) dbg += $"[{f}]\n";
			// Debug.Log( $"[CSV] evalling [{str}]\n\n{dbg}" );

			return fields.ToArray();
		}


		public static string ToClockString( this float seconds ) => $"{(int)(seconds/60)%60:00}:{(int)seconds%60:00}:{(int)(seconds*100)%100:00}";


		public static void SetOrAdd<Tkey,Tvalue>( this Dictionary<Tkey,Tvalue> dict, Tkey key, Tvalue value )
		{
			if( dict.ContainsKey(key) ) dict[key] = value;
			else dict.Add( key, value );
		}
		public static Tvalue GetOrAdd<Tkey,Tvalue>( this Dictionary<Tkey,Tvalue> dict, Tkey key ) where Tvalue : class, new()
		{
			if( dict.ContainsKey(key) ) return dict[key];
			Tvalue result = new Tvalue();
			dict.Add( key, result );
			return result;
		}
		public static void AddUp<T>( this Dictionary<T,int> dict, T key )
		{
			if( dict.ContainsKey(key) ) dict[key]++;
			else dict.Add(key,1);
		}

		public static TValue Get<T,TValue>( this Dictionary<T,TValue> dict, T key )
		{
			if( dict.ContainsKey(key) ) return dict[key];
			else return default(TValue);
		}


		//
		// special Dictionaries
		//
		public static void AddOrPut<T,TElm>( this Dictionary<T,HashSet<TElm>> dict, T key, TElm element )
		{
			if( dict.ContainsKey( key ) == false ) dict.Add( key, new HashSet<TElm>() );
			dict[key].Add( element );
		}
		public static void RemoveOrClear<T,TElm>( this Dictionary<T,HashSet<TElm>> dict, T key, TElm element )
		{
			if( dict.ContainsKey( key ) == false ) return;
			dict[key].Remove( element );
			if( dict[key].Count == 0 ) dict.Remove( key );
		}



		public static T Get<T>( this List<T> list, int idx )
		{
			if( idx < list.Count ) return list[idx];
			else return default(T);
		}


		public static T PickOne<T>( this T[] a ) => a[Random.Range(0,a.Length)];
		public static T PickOne<T>( this List<T> list ) => list.Count>0?list[Random.Range(0,list.Count)]:default;


		//
		// spawn helpers
		//
		public static GameObject Spawn( this GameObject prefab )
		{
			var newOp = GameObject.Instantiate( prefab );
			newOp.SetActive( true );
			return newOp;
		}
		public static GameObject SpawnAt( this GameObject prefab, GameObject other, bool scale = false )
			=> prefab.SpawnAt( other.transform, scale );
		public static GameObject SpawnAt( this GameObject prefab, Component other, bool scale = false )
		{
			var newOp = Spawn( prefab );
			if( scale ) newOp.transform.Mimic( other.transform );
			else newOp.transform.Pose( other.transform );
			newOp.transform.position = other.transform.position;
			newOp.transform.rotation = other.transform.rotation;
			return newOp;
		}
		public static GameObject SpawnSibling( this GameObject source, bool scale = true ) =>
			SpawnInside( source, source.transform.parent, scale );
		public static GameObject SpawnInside( this GameObject prefab, Transform transform, bool scale = true )
		{
			// we want to immediately parent, because oftentimes the spawned Awake/OnEnable functions may want to lookup
			// something in their parents immediately
			var newOp = GameObject.Instantiate( prefab, transform );
			if( scale ) newOp.transform.Zero();
			else
			{
				// keeping the scale of the prefab becomes a little awkard with the immediate parenting though...
				newOp.transform.SetParent(null,true);
				newOp.transform.localScale = prefab.transform.localScale;
				newOp.transform.Pose(transform);
				newOp.transform.SetParent( transform, true );
			}
			newOp.SetActive( true );
			return newOp;
		}


		public static void Copy( this Transform transform, Transform target )
		{
			var p = transform.parent;
			transform.SetParent( target );
			transform.Zero();
			transform.SetParent( p, true );
		}

	}
}